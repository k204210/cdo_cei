#!/bin/sh
#Program:
cdo=/work/bm0021/cdo_climateindices/cdo_cei/bin/cdo
#
#Switches:
#
prepareTimeseries=Y
#
fd=Y
su=Y
id=Y
tr=Y
r10=Y
r1=Y
r20=Y
#
#Raw directory:
#
root=/work/ik1017/CMIP6/data/CMIP6/
activity_id=ScenarioMIP
institution_id=DKRZ
source_id=MPI-ESM1-2-HR
experiment_id=ssp585
member_id=r1i1p1f1
version=v20190710
#
startyear=2071
endyear=2100
#
#Raw Files:
#
rawTasmax=${root}/${activity_id}/${institution_id}/${source_id}/${experiment_id}/${member_id}/day/tasmax/gn/${version}/
rawTasmin=${root}/${activity_id}/${institution_id}/${source_id}/${experiment_id}/${member_id}/day/tasmin/gn/${version}/
rawPr=${root}/${activity_id}/${institution_id}/${source_id}/${experiment_id}/${member_id}/day/pr/gn/${version}/
#
#Merged timeseries
#
mergedDir=timeseries/
mkdir -p $mergedDir
#
tasmaxMerged=${mergedDir}/tasmax_${source_id}_${experiment_id}_${member_id}_${startyear}-${endyear}.nc
tasminMerged=${mergedDir}/tasmin_${source_id}_${experiment_id}_${member_id}_${startyear}-${endyear}.nc
prMerged=${mergedDir}/pr_${source_id}_${experiment_id}_${member_id}_${startyear}-${endyear}.nc
#
if [[ $prepareTimeseries ]]; then
  for yearfrag in `seq ${startyear:0:3} 1 ${endyear:0:3}`; do
    mergetermTasmax="${mergetermTasmax} ${rawTasmax}*${yearfrag}*"
    mergetermTasmin="${mergetermTasmin} ${rawTasmin}*${yearfrag}*"
    mergetermPr="${mergetermPr} ${rawPr}*${yearfrag}*"
  done
  $cdo selyear,${startyear}/${endyear} -mergetime [ ${mergetermTasmax} ] ${tasmaxMerged}
  $cdo selyear,${startyear}/${endyear} -mergetime [ ${mergetermTasmin} ] ${tasminMerged}
  $cdo selyear,${startyear}/${endyear} -mergetime [ ${mergetermPr} ] ${prMerged}
fi
#
#Climate indices:
cdoOutput=cdooutput/
#
#frost days
if [[ $fd ]]; then
  cdoFd=$cdoOutput/fd_yr_${source_id}_${experiment_id}_${member_id}_${startyear}-${endyear}.nc
  $cdo etccdi_fd ${tasminMerged} ${cdoFd}
fi
#summer days
if [[ $su ]]; then
  cdoSu=$cdoOutput/su_yr_${source_id}_${experiment_id}_${member_id}_${startyear}-${endyear}.nc
  $cdo etccdi_su ${tasmaxMerged} ${cdoSu}
fi
#ice days
if [[ $id ]]; then
  cdoId=$cdoOutput/id_yr_${source_id}_${experiment_id}_${member_id}_${startyear}-${endyear}.nc
  $cdo etccdi_id ${tasmaxMerged} ${cdoId}
fi
#tropical nights
if [[ $tr ]]; then
  cdoTr=$cdoOutput/tr_yr_${source_id}_${experiment_id}_${member_id}_${startyear}-${endyear}.nc
  $cdo etccdi_tr ${tasminMerged} ${cdoTr}
fi
#Annual count of days when PRCP≥ xmm
#r1mm
if [[ $r1 ]]; then
  cdoR1=$cdoOutput/r1mm_yr_${source_id}_${experiment_id}_${member_id}_${startyear}-${endyear}.nc
  $cdo etccdi_r1mm -mulc,86400 ${prMerged} ${cdoR1}
fi
#r10mm
if [[ $r10 ]]; then
  cdoR10=$cdoOutput/r10mm_yr_${source_id}_${experiment_id}_${member_id}_${startyear}-${endyear}.nc
  $cdo etccdi_r10mm -mulc,86400 ${prMerged} ${cdoR10}
fi
#r20mm
if [[ $r20 ]]; then
  cdoR20=$cdoOutput/r20mm_yr_${source_id}_${experiment_id}_${member_id}_${startyear}-${endyear}.nc
  $cdo etccdi_r20mm -mulc,86400 ${prMerged} ${cdoR20}
fi



