#!/bin/sh
#Program:
cdo=/work/bm0021/cdo_climateindices/cdo_cei/bin/cdo
#
#Switches:
#
#prepareTimeseries=Y
#
prepareHistogram=Y
#
#tx90p=Y
#tx10p=Y
#tn90p=Y
tn10p=Y
#r95p=Y
#r99p=Y
#
#Raw directory:
#
root=/work/ik1017/CMIP6/data/CMIP6/
activity_id=ScenarioMIP
#activity_id=CMIP
institution_id=DKRZ
#institution_id=MPI-M
source_id=MPI-ESM1-2-HR
experiment_id=ssp585
#experiment_id=historical
member_id=r1i1p1f1
version=v20190710
#
#startyear=1961
startyear=2071
#endyear=1990
endyear=2100
#
#Raw Files:
#
rawTasmax=${root}/${activity_id}/${institution_id}/${source_id}/${experiment_id}/${member_id}/day/tasmax/gn/${version}/
rawTasmin=${root}/${activity_id}/${institution_id}/${source_id}/${experiment_id}/${member_id}/day/tasmin/gn/${version}/
rawPr=${root}/${activity_id}/${institution_id}/${source_id}/${experiment_id}/${member_id}/day/pr/gn/${version}/
#
cdoOutput=cdooutput/
#
#Merged timeseries
#
mergedDir=timeseries/
mkdir -p $mergedDir
#
tasmaxMerged=${mergedDir}/tasmax_${source_id}_${experiment_id}_${member_id}_${startyear}-${endyear}.nc
tasminMerged=${mergedDir}/tasmin_${source_id}_${experiment_id}_${member_id}_${startyear}-${endyear}.nc
prMerged=${mergedDir}/pr_${source_id}_${experiment_id}_${member_id}_${startyear}-${endyear}.nc
#
#Running mins and running max for histograms:
#
tasminrunmin=${cdoOutput}/tasmin_runmin_${source_id}_${experiment_id}_${member_id}_${startyear}-${endyear}.nc
tasminrunmax=${cdoOutput}/tasmin_runmax_${source_id}_${experiment_id}_${member_id}_${startyear}-${endyear}.nc
tasmaxrunmin=${cdoOutput}/tasmax_runmin_${source_id}_${experiment_id}_${member_id}_${startyear}-${endyear}.nc
tasmaxrunmax=${cdoOutput}/tasmax_runmax_${source_id}_${experiment_id}_${member_id}_${startyear}-${endyear}.nc
prtimmin=${cdoOutput}/pr_timmin_${source_id}_${experiment_id}_${member_id}_${startyear}-${endyear}.nc
prtimmax=${cdoOutput}/pr_timmax_${source_id}_${experiment_id}_${member_id}_${startyear}-${endyear}.nc
#
#Define Options for the operator:
#
#Running window days:
window=5
#Maximal number of OpenMP threads:
threads="32"
#Memory precision:
#isFloat="--float"
#Output frequency:
#isMonthlyOutput=",m"
#Number of bins for histogram:
nbins="$((window*(endboot-startboot+1)*2+2))"
#
if [[ $prepareTimeseries ]]; then
  for yearfrag in `seq ${startyear:0:3} 1 ${endyear:0:3}`; do
    mergetermTasmax="${mergetermTasmax} ${rawTasmax}*${yearfrag}*"
    mergetermTasmin="${mergetermTasmin} ${rawTasmin}*${yearfrag}*"
    mergetermPr="${mergetermPr} ${rawPr}*${yearfrag}*"
  done
  $cdo selyear,${startyear}/${endyear} -mergetime [ ${mergetermTasmax} ] ${tasmaxMerged}
  $cdo selyear,${startyear}/${endyear} -mergetime [ ${mergetermTasmin} ] ${tasminMerged}
  $cdo selyear,${startyear}/${endyear} -mergetime [ ${mergetermPr} ] ${prMerged}
fi
#
#Climate indices:
#
#Histogram limits
#
if [[ $prepareHistogram ]]; then
  $cdo ydrunmin,5,rm=c $tasminMerged $tasminrunmin
  $cdo ydrunmax,5,rm=c $tasminMerged $tasminrunmax
  $cdo ydrunmin,5,rm=c $tasmaxMerged $tasmaxrunmin
  $cdo ydrunmax,5,rm=c $tasmaxMerged $tasmaxrunmax
  $cdo timmin -setrtomiss,0,1 -mulc,86400 $prMerged $prtimmin
  $cdo timmax -setrtomiss,0,1 -mulc,86400 $prMerged $prtimmax
fi
#
#Percentage of Days when Daily Maximum Temperature is Above the 90th Percentile
#
if [[ $tx90p ]]; then
  export CDO_PCTL_NBINS=${nbins}
  cdoTx90p=$cdoOutput/tx90p_yr_${source_id}_${experiment_id}_${member_id}_${startyear}-${endyear}.nc
  $cdo $isFloat -v -P $threads etccdi_tx90p,$window,${startyear},$endyear,freq=year $tasmaxMerged $tasmaxrunmin $tasmaxrunmax ${cdoTx90p}
fi
if [[ $tx10p ]]; then
  export CDO_PCTL_NBINS=${nbins}
  cdoTx10p=$cdoOutput/tx10p_yr_${source_id}_${experiment_id}_${member_id}_${startyear}-${endyear}.nc
  $cdo $isFloat -v -P $threads etccdi_tx10p,$window,${startyear},$endyear,freq=year $tasmaxMerged $tasmaxrunmin $tasmaxrunmax ${cdoTx10p}
fi
if [[ $tn90p ]]; then
  export CDO_PCTL_NBINS=${nbins}
  cdoTn90p=$cdoOutput/tn90p_yr_${source_id}_${experiment_id}_${member_id}_${startyear}-${endyear}.nc
  $cdo $isFloat -v -P $threads etccdi_tn90p,$window,${startyear},$endyear,freq=year $tasminMerged $tasminrunmin $tasminrunmax ${cdoTn90p}
fi
if [[ $tn10p ]]; then
  export CDO_PCTL_NBINS=${nbins}
  cdoTn10p=$cdoOutput/tn10p_yr_${source_id}_${experiment_id}_${member_id}_${startyear}-${endyear}.nc
  $cdo $isFloat -v -P $threads etccdi_tn10p,$window,${startyear},$endyear,freq=year $tasminMerged $tasminrunmin $tasminrunmax ${cdoTn10p}
fi
if [[ $r95p ]]; then
  export CDO_PCTL_NBINS=${nbins}
  prtemp=prtemp.nc
  $cdo -setrtomiss,0,1 -mulc,86400 $prMerged $prtemp
  cdor95p=$cdoOutput/r95p_yr_${source_id}_${experiment_id}_${member_id}_${startyear}-${endyear}.nc
  $cdo $isFloat -v -P $threads etccdi_r95p,${startyear},$endyear,freq=year $prtemp $prtimmin $prtimmax ${cdor95p}
  rm $prtemp
fi
if [[ $r99p ]]; then
  export CDO_PCTL_NBINS=${nbins}
  prtemp=prtemp.nc
  $cdo -setrtomiss,0,1 -mulc,86400 $prMerged $prtemp
  cdor99p=$cdoOutput/r99p_yr_${source_id}_${experiment_id}_${member_id}_${startyear}-${endyear}.nc
  $cdo $isFloat -v -P $threads etccdi_r99p,${startyear},$endyear,freq=year $prtemp $prtimmin $prtimmax ${cdor99p}
  rm $prtemp
fi





