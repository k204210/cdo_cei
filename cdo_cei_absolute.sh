#!/bin/sh
#Program:
cdo=/work/bm0021/cdo_climateindices/cdo_cei/bin/cdo
#
#Switches:
#
prepareTimeseries=Y
#
tnn=Y
tnx=Y
txn=Y
txx=Y
sdii=Y
dtr=Y
rx1day=Y
rx5day=Y
prcptot=Y
#
#Raw directory:
#
root=/work/ik1017/CMIP6/data/CMIP6/
activity_id=ScenarioMIP
institution_id=DKRZ
source_id=MPI-ESM1-2-HR
experiment_id=ssp585
member_id=r1i1p1f1
version=v20190710
#
startyear=2071
endyear=2100
#
#Raw Files:
#
rawTasmax=${root}/${activity_id}/${institution_id}/${source_id}/${experiment_id}/${member_id}/day/tasmax/gn/${version}/
rawTasmin=${root}/${activity_id}/${institution_id}/${source_id}/${experiment_id}/${member_id}/day/tasmin/gn/${version}/
rawPr=${root}/${activity_id}/${institution_id}/${source_id}/${experiment_id}/${member_id}/day/pr/gn/${version}/
#
#Merged timeseries
#
mergedDir=timeseries/
mkdir -p $mergedDir
#
tasmaxMerged=${mergedDir}/tasmax_${source_id}_${experiment_id}_${member_id}_${startyear}-${endyear}.nc
tasminMerged=${mergedDir}/tasmin_${source_id}_${experiment_id}_${member_id}_${startyear}-${endyear}.nc
prMerged=${mergedDir}/pr_${source_id}_${experiment_id}_${member_id}_${startyear}-${endyear}.nc
#
if [[ $prepareTimeseries ]]; then
  for yearfrag in `seq ${startyear:0:3} 1 ${endyear:0:3}`; do
    mergetermTasmax="${mergetermTasmax} ${rawTasmax}*${yearfrag}*"
    mergetermTasmin="${mergetermTasmin} ${rawTasmin}*${yearfrag}*"
    mergetermPr="${mergetermPr} ${rawPr}*${yearfrag}*"
  done
  $cdo selyear,${startyear}/${endyear} -mergetime [ ${mergetermTasmax} ] ${tasmaxMerged}
  $cdo selyear,${startyear}/${endyear} -mergetime [ ${mergetermTasmin} ] ${tasminMerged}
  $cdo selyear,${startyear}/${endyear} -mergetime [ ${mergetermPr} ] ${prMerged}
fi
#
#Climate indices:
#
cdoOutput=cdooutput/
mkdir -p ${cdoOutput}
#minimum of daily minimum temperature each year
if [[ $tnn ]]; then
  cdoTnn=$cdoOutput/tnn_yr_${source_id}_${experiment_id}_${member_id}_${startyear}-${endyear}.nc
  $cdo subc,273.15 -yearmin ${tasminMerged} $cdoTnn
fi
#
#maximum of daily minimum temperature each year
if [[ $tnx ]]; then
  cdoTnx=$cdoOutput/tnx_yr_${source_id}_${experiment_id}_${member_id}_${startyear}-${endyear}.nc
  $cdo subc,273.15 -yearmax ${tasminMerged} $cdoTnx
fi
#
#minimum of daily maximum temperature each year
if [[ $txn ]]; then
  cdoTxn=$cdoOutput/txn_yr_${source_id}_${experiment_id}_${member_id}_${startyear}-${endyear}.nc
  $cdo subc,273.15 -yearmin ${tasmaxMerged} $cdoTxn
fi
#
#maximum of daily maximum temperature each year
if [[ $txx ]]; then
  cdoTxx=$cdoOutput/txx_yr_${source_id}_${experiment_id}_${member_id}_${startyear}-${endyear}.nc
  $cdo subc,273.15 -yearmax ${tasmaxMerged} $cdoTxx
fi
#
#diurnal temperature range
if [[ $dtr ]]; then
  cdoDtr=$cdoOutput/dtr_yr_${source_id}_${experiment_id}_${member_id}_${startyear}-${endyear}.nc
  $cdo yearmean -sub ${tasmaxMerged} ${tasminMerged} $cdoDtr
fi
#
#Highest one day PRCP
if [[ $rx1day ]]; then
  cdoRx1day=$cdoOutput/rx1day_yr_${source_id}_${experiment_id}_${member_id}_${startyear}-${endyear}.nc
  $cdo etccdi_rx1day -mulc,86400 ${prMerged} $cdoRx1day
fi
#
#Highest five day PRCP
if [[ $rx5day ]]; then
  cdoRx5day=$cdoOutput/rx5day_yr_${source_id}_${experiment_id}_${member_id}_${startyear}-${endyear}.nc
  CDO_TIMESTAT_DATE="last" $cdo etccdi_rx5day -runsum,5 -mulc,86400 ${prMerged} $cdoRx5day
fi
#
#Simple daily intensity index
if [[ $sdii ]]; then
  cdosdii=$cdoOutput/sdii_yr_${source_id}_${experiment_id}_${member_id}_${startyear}-${endyear}.nc
  $cdo etccdi_sdii -mulc,86400 ${prMerged} $cdosdii
fi
#
#total wet-day precip
if [[ $prcptot ]]; then
  cdoPrcptot=$cdoOutput/prcptot_yr_MPI-ESM-LR_historical_r1i1p1_${startyear}-${endyear}.nc
  $cdo mulc,86400 -yearsum -mul ${prMerged} -gtc,1 -mulc,86400 ${prMerged} $cdoPrcptot
fi
#
